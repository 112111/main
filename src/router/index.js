import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)


export default new Router({
    mode:'history',
    base:process.env.BASE_URL,
    routes:[
        {
            path: '/',
            name: 'home',
            component: () => import('../views/MainPage')
        },
        {
            path: '/ContestsPage',
            name: 'ContestsPage',
            component: () => import('../views/ContestsPage')
        },
        {
            path: '/DocumentsPage',
            name: 'DocumentsPage',
            component: () => import('../views/DocumentsPage')
        },
        {
            path: '/EmployeesPage',
            name: 'EmployeesPage',
            component: () => import('../views/EmployeesPage')
        },
        {
            path: '/MipsPage',
            name: 'MipsPage',
            component: () => import('../views/MipsPage')
        },
        {
            path: '/ProjectPage',
            name: 'ProjectPage',
            component: () => import('../views/ProjectPage')
        },
        {
            path: '/ProjectsPage',
            name: 'ProjectsPage',
            component: () => import('../views/ProjectsPage')
        },
        {
            path: '/RegistryPage',
            name: 'RegistryPage',
            component: () => import('../views/RegistryPage')
        }
    ]
})